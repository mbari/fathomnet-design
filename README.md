# FathomNet Design

Repository to organize design documents for FathomNet. User stories are being registered in [Issues](https://bitbucket.org/mbari/fathomnet-design/issues)

## External Documents

- [Merged User Stories](https://docs.google.com/document/d/1CyC7QcdOZAbrwBHQ1-HAKmQcutM17hF2MH4TcWKQOvw/edit?pli=1#heading=h.ubypo89kucbj)
- [Kakani's FathomNet Trello Board](https://trello.com/b/mEsuWWCX/fathomnet)
- [VARS Annotation Assistance Software Requirements](https://docs.google.com/document/d/1LWQHAjs7uFXmgdrIXF7aQVmDer86XHTdpqUbKZYIy5o/edit)

![Data Model](docs/diagrams/FathomNet-ReadDataModel.png)